# Итоговый проект курса DevOps инженер от компании Skillbox
## Этот репозиторий содержит файлы для развёртывания приложения

Файлы создающие инфраструктуру находятся в другом [репозитории](https://gitlab.com/Dovinant/skillbox-infra).
Для работы потребуется [Docker Community Collection](https://galaxy.ansible.com/community/docker).
Файлы этого репозитория:
- ansible.cfg - файл настройки ansible.
- deploy.yml - playbook запускающий контейнер докера.
- unit_template.j2 - шаблон unit файла. Запускает созданный контейнер с помощью systemd.
- group_vars/web_servers.yml - переменные.
- hosts - содержит две группы web_servers_TEST и web_servers_PROD. Сервис разворачивается на хосты из этих групп.
- var.yml -  содержит скрипт присваивающий значение переменной _env_. Само значение передаёт .gitlab-ci из репозитория [skillbox-diploma](https://gitlab.com/Dovinant/skillbox-diploma) (смотри блок "deploy_to_prod:" файла .gitlab-ci)

Работу playbook определяют переменные из файла group_vars/web_servers.yml
- ansible_user: имя под которым ansible подключается к серверу
- ansible_ssh_private_key_file: путь к приватному ключу ssh
- image_name: имя докер образа
- image_tag: тэг докер образа
- service_name: имя сервиса под которым будет запускаться докер контейнер
- container_name: имя докер контейнера
- docker_username: логин на сайте hub.docker.com
- docker_password: пароль на сайте hub.docker.com

Значение переменной docker_password зашифровано с помощью Ansible Vault.

Наконец, инвентарный файл hosts содержит FQDN хостов на которых будет развёрнут контейнер. Имя хостов в DNS прописывает Terraform на этапе [развёртывания инфраструктуры](https://gitlab.com/Dovinant/skillbox-infra). Путь к файлу hosts определён в ansible.cfg.

### Общий принцип работы такой

Playbook можно запустить локально командой:
> `ansible-playbook deploy.yml --vault-password-file=файл_с_паролем` 
или
> `ansible-playbook --vault-id @prompt deploy.yml` 

Но сейчас в проекте [skillbox-diploma](https://gitlab.com/Dovinant/skillbox-diploma) реализован pipeline который клонирует этот репозиторий  на сервер с gitlab-runner, после чего автоматически запускает playbook.

В любом случае playbook сделает следующее:
- подключится к докер хаб
- на сервер прописанный в hosts скачает образ определённый переменными _docker_username_, _image_name_, _image_tag_
- создаст контейнер с именем _container_name_
- создаст unit-файл с именем $service_name.servce для запуска контейнера через systemd

### Как работает pipeline в проекте skillbox-diploma

После создания и настройки инфраструктуры (см. проект [skillbox-infra](https://gitlab.com/Dovinant/skillbox-infra)), к репозиторию [skillbox-diploma](https://gitlab.com/Dovinant/skillbox-diploma) поключаются два раннера:

- Example Shell GitLab Runner - executor: shell, tags: shell
- Example Docker GitLab Runner - executor: docker, tags: docker, mysql, node, ruby

Для skillbox-diploma определены четыре переменных (Settings > CI/CD > Variables):

- CI_REGISTRY_USER = имя пользователя на Docker HUB
- CI_REGISTRY_PASSWORD = пароль на Docker HUB
- CI_REGISTRY = docker.io
- CI_REGISTRY_IMAGE = index.docker.io/username/image_name

В файле [.gitlab-ci](https://gitlab.com/Dovinant/skillbox-diploma/-/blob/main/.gitlab-ci.yml) определены три задачи:

- test - выполняет Example Shell GitLab Runner, запуск тестов. Задача выполняется для всех веток.
- buid - выполняет Example Docker GitLab Runner, регистрируется на hub.docker.com, собирает образ докер и заливает его на докер хаб. Задача выполняется только для ветки _uat_.
- deploy - выполняет Example Shell GitLab Runner, клонирует на сервер runner репозиторий skillbox-service, клонирует туда же приватный репозиторий deploy-secrets, устанавливает зависимости для ansible и запускает плейбук deploy.yml. Задача выполняется только для ветки _uat_.

Приватный репозиторий deploy-secrets содержит ключи ssh и файл с паролем для Ansible Vault. Репозиторий клонирует специальный пользователь _gitlab-ci-token_ с динамическим [CI_JOB_TOKEN](https://github.com/gitlabhq/gitlabhq/blob/master/doc/ci/jobs/ci_job_token.md#gitlab-cicd-job-token-free). Чтобы из pipeline был доступ к приватному репозиторию deploy-secrets в настройках deploy-secrets надо добавить репозиторий skillbox-diploma в [allowlist](https://github.com/gitlabhq/gitlabhq/blob/master/doc/ci/jobs/ci_job_token.md#add-a-project-to-the-job-token-scope-allowlist)

### Примечания

- Все три задачи можно было бы выполнить на одном раннере Example Shell GitLab Runner. Работа с Example Docker GitLab Runner сделана просто для примера.
- Если работу выполняет Example Docker GitLab Runner все необходимые зависимости надо установить в докер контейнер. Так, например, для задачи deploy надо будет прописать в .gitlab-ci:
```
deploy:
  stage: deploy
  before_script:
    - apk update && apk add git ansible
  .....

```

## Дополнительные материалы

### Ansible
- [Community.Docker](https://docs.ansible.com/ansible/latest/collections/community/docker/index.html)
- [Расширяем возможности Ansible: Ansible Vault](https://habr.com/ru/companies/otus/articles/722106/)
- [Как собрать Docker-контейнеры с помощью Ansible](https://habr.com/ru/companies/southbridge/articles/656201/)
- [Как запустить Docker / Podman контейнеры в качестве службы Systemd](https://itsecforu.ru/2020/04/28/%F0%9F%90%B3-%D0%BA%D0%B0%D0%BA-%D0%B7%D0%B0%D0%BF%D1%83%D1%81%D1%82%D0%B8%D1%82%D1%8C-docker-podman-%D0%BA%D0%BE%D0%BD%D1%82%D0%B5%D0%B9%D0%BD%D0%B5%D1%80%D1%8B-%D0%B2-%D0%BA%D0%B0%D1%87%D0%B5/)
- [Автоматический запуск Docker контейнера](https://itisgood.ru/2022/06/23/avtomaticheskij-zapusk-docker-kontejnera/)

### Docker
- [Введение в Docker Hub и все, что вы должны знать о нем](https://itgap.ru/post/docker-hub-vvedenie)
- [Publish a Docker image to Docker Hub using GitLab](https://dev.to/mattdark/publish-a-docker-image-to-docker-hub-using-gitlab-2b9o)

### GitLab
- [GitLab CI/CD job token (FREE)](https://github.com/gitlabhq/gitlabhq/blob/master/doc/ci/jobs/ci_job_token.md#gitlab-cicd-job-token-free)
